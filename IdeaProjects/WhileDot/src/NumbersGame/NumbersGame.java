package NumbersGame;

/**
 * Игра "Угадай число"
 *
 * Created by artwhite on 24.01.17.
 */

import java.util.Scanner;

import static java.lang.System.exit;

public class NumbersGame {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int randNum = (1 * (int) (Math.random() * 10)); // загаданное рандомное число
        startGame(randNum);                             // начало игры
    }

    /**
     * Метод просит ввести число пока пользователь не угадает
     *
     * @param randNum
     */
    private static void startGame(int randNum) {
        System.out.print("Угадай число - ");

        int myNum = sc.nextInt();
        if (myNum != randNum) {
            do {
                if (myNum < randNum){
                    System.out.print("Загаднное число больше - ");
                    myNum = sc.nextInt();
                } else if(myNum > randNum){
                    System.out.print("Загаднное число меньше - ");
                    myNum = sc.nextInt();
                }
            } while (myNum != randNum);
        }
        System.out.println("Ты угадал, поздравляю!");
    }
}
