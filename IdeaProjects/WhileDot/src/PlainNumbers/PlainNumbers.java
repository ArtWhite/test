package PlainNumbers;

/**
 * Вывод простых чисел от 2 до 100
 *
 * Created by artwhite on 20.01.17.
 */
public class PlainNumbers {
    public static void main(String[] args) {

        for(int i = 2; i <= 100; i++){
            boolean isPlain = isPrime(i);
            if(isPlain){
                System.out.println(i);
            }
        }



    }

    /**
     * Проверка на то, является ли число простым
     */
    public static boolean isPrime(int N) {
        if (N < 2) return false;
        for (int i = 2; i*i <= N; i++)
            if (N % i == 0) return false;
        return true;
    }

}
