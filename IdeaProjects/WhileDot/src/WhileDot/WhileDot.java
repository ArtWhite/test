package WhileDot;

/**
 * Подсчет пробелов пока не встретится точка
 *
 * Created by artwhite on 17.01.17.
 */
import java.util.Scanner;
public class WhileDot {

    static Scanner sc = new Scanner(System.in);


    public static void main(String args[]){
        System.out.print("Введите предложение - ");
        String words = sc.nextLine();

        dots(words);
    }

    /**
     * Метод выводит кол-во пробелов если не встречается точка
     *
     * @param words
     */

    public static void dots(String words){
        char dot;
        boolean beDot = false;

        int spaces = 0;
        for(int i = 0; i < words.length(); i++){
            dot = words.charAt(i);

            if(dot == ' '){
                spaces++;
            }
            else if(dot == '.'){
                beDot = true;
                break;
            }
        }
        if (beDot){
            System.out.println("Встретилась точка, кол-во пробелов = " + spaces);
        } else {
            System.out.println("Точка не встретилась, кол-во пробелов = " + spaces);
        }

    }

}
