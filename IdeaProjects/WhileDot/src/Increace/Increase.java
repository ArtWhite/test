package Increace;

/**
 * Created by artwhite on 20.01.17.
 */
import java.util.Scanner;
public class Increase {
    static Scanner sc = new Scanner(System.in);
    public static void main(String args[]){
        System.out.print("Введите количество элементов в массиве - ");
        int countOfArray = sc.nextInt();        // Кол-во элементов в массиве
        int array[] = new int[countOfArray];    // массив

        filling(array);
        increace(array);

    }


    /**
     * Метод заменяет элемент в массиве
     *
     * @param array
     */

    static void increace(int[] array) {
        System.out.print("Какой элемнет желаете заменить? - ");
        int element = sc.nextInt();
        int old = array[element-1];
        float result = ((float) array[element-1]*110)/100;

        System.out.printf("Заменен " + (element) + " элемент c " + old + " на %.2f", result);

    }

    /**
     * Метод рандомно заполняет массив
     *
     * @param array
     */

    private static void filling(int[] array) {
        for (int i = 0; i < array.length; i++){
            array[i] = 1 * (int)(Math.random()*700);
        }
        inputFilling(array);
    }

    /**
     * Метод выводит массив
     *
     * @param array
     */

    private static void inputFilling(int[] array) {
        for (int i = 0; i < array.length; i++){
            System.out.println(array[i]);
        }
    }
}
