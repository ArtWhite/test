package Matrix;

/**
 * Обнуление столбца матрицы матрицы
 *
 * Created by artwhite on 24.01.17.
 */

import java.util.Scanner;
public class Matrix {
    static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        int matrix[][] = new int[10][10]; // Матрица

        filling(matrix);

        change(matrix);

    }

    /**
     * Метод обнуляет столбец
     *
     * @param matrix
     */

    private static void change(int[][] matrix) {
        System.out.print("Какой столбец хотите заменить? - ");
        int column = sc.nextInt();
        for (int i = 0; i < matrix.length; i++){
            for (int n = 0; n < matrix[i].length; n++){
                matrix[i][column-1] = 0;
                System.out.print(matrix[i][n] + "\t");
            }System.out.println();
        }

    }

    /**
     * Метод для заполнения матрицы
     *
     * @param matrix
     */

    private static void filling(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++){
            for (int n = 0; n < matrix[i].length; n++){
                matrix[i][n] = 1*(int)(Math.random()*10);
                System.out.print(matrix[i][n] + "\t");
            }System.out.println();
        }
    }
}
