package ChangeMoney;

/**
 * Перевод рублей в деньги
 *
 * Created by artwhite on 20.01.17.
 */
import java.util.Scanner;
public class ChangeMoney {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите сколько у вас рублей - ");
        double rubles = sc.nextFloat();             // Кол-во рублей
        if(rubles <= 0){
            System.out.print("Тут менять нечего");
            System.exit(0);
        }
        System.out.print("Ввседите курс Евро - ");
        double course;                              // Курс Евро
        do {
            course = sc.nextFloat();
        } while(course <= 0);



        double moneys = change(rubles,course);

        System.out.printf("У вас %.2f Евро", moneys);
    }

    /**
     * Метод возвращает деньги преведенные по курсу
     *
     * @param rubles
     * @param course
     * @return moneys
     */

    private static double change(double rubles, double course) {
        double moneys;

        moneys = rubles/course;

        return moneys;
    }
}
