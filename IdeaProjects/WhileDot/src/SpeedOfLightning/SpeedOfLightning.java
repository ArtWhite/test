package SpeedOfLightning;

/**
 * Рассчет расстяония от удара молнии
 *
 * Created by artwhite on 20.01.17.
 */
public class SpeedOfLightning {
    public static void main(String[] args) {
        double speed = 1234.8;          // Скорость молнии
        double interval = 6.8;          // Интервал

        double speedPerSecond = (1234.8/60)/60.0;   // Скорость молнии в секунду

        double result = calc(speedPerSecond, interval);    //

        System.out.printf("Молния ударила на расстоянии %.3fкм", result);
    }

    /**
     * Метод рассчитывает и выводит расстояние
      */

    private static double calc(double speedPerSecond, double interval) {
        double res = speedPerSecond * interval;

        return res;
    }
}
