package TotalDouble;

/**
 * Является ли чсило типа double целым
 *
 * Created by artwhite on 24.01.17.
 */

import java.util.Scanner;
public class TotalDouble {
    static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.print("Введите число типа double -" );
        double num = sc.nextDouble(); // введеное число

        if (num%1 != 0.00){
            System.out.print("Не целое");
        } else {
            System.out.print("Целое");
        }
    }
}
