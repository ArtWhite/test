package multiplicationTable;

/**
 * Created by artwhite on 24.01.17.
 */

import java.util.Scanner;

public class MultiplicationTable {
    static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.print("Введите число, которое хотитет умножить - ");
        int num = sc.nextInt(); // Число для таблицы
        multiplication(num);

    }

    /**
     * Метод выводит таблтцу умножения
     *
     * @param num
     */

    private static void multiplication(int num) {
        for (int i = 1; i <= 10; i++){
            System.out.println(num + "X" + i + " = " + (i*num));
        }
    }

}
